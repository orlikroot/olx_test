To change tests setting, use test.properties file.

To change counts of test use testng.xml configuration file.

Run test with Maven: mvn test

Report directory: target/surefire-reports/index.html

Default browser timeout - 4 sec.
if you have slow internet connection please increase timeout (Uncomment Configuration.timeout and set new value in milliseconds)