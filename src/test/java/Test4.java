import com.codeborne.selenide.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.id;

/**
 * Created by EvgenK on 9/19/2017.
 */
@Slf4j
public class Test4 {

    private final ResourceBundle testData = ResourceBundle.getBundle("test");

    private final String Browser = testData.getString("Browser");
    private final String URL = testData.getString("URL");
    private final String login = testData.getString("Login");
    private final String password = testData.getString("Password");

    @BeforeClass
    public void preCondition() {
        Configuration.browser = Browser;
//        Configuration.timeout = 5000;
//        Configuration.holdBrowserOpen = true;

        open(URL);
    }

    @Test(description = "Delete user")
    public void deleteUserTest() {
        $(id("topLoginLink")).click();
        if (!$(id("userLoginBox")).is(exist)) {
            $(id("userEmail")).setValue(login);
            $(id("userPass")).setValue(password);
            $(id("se_userLogin")).click();
        }

        $(id("se_accountShop")).click();
        $(id("se_deleteAccount")).click();

        $(id("deleteAccount")).click();


        Assert.assertTrue($(id("removeInput")).shouldBe(exist).exists());
        Assert.assertTrue($(id("removeInput")).parent().shouldHave(cssClass("alert")).exists());
    }
}
