import com.codeborne.selenide.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.*;

/**
 * Created by EvgenK on 9/19/2017.
 */
@Slf4j
public class Test1 {

    private final ResourceBundle testData = ResourceBundle.getBundle("test");

    private final String Browser = testData.getString("Browser");
    private final String URL = testData.getString("URL");
    private final String login = testData.getString("Login");
    private final String password = testData.getString("Password");

    private final String TITLE = testData.getString("AdTitle");
    private final String TEXT = testData.getString("AdDescription");

    @BeforeClass
    public void preCondition() {
        Configuration.browser = Browser;
//        Configuration.timeout = 5000;
//        Configuration.holdBrowserOpen = true;

        open(URL);
    }

    @Test(description = "Add new advertisement")
    public void addNewAdTest() {
        $(id("topLoginLink")).click();
        if (!$(id("userLoginBox")).is(exist)) {
            $(id("userEmail")).setValue(login);
            $(id("userPass")).setValue(password);
            $(id("se_userLogin")).click();
        }

        $(id("postNewAdLink")).click();

        $(id("add-title")).setValue(TITLE);
        $(id("choose-category-ilu")).click();
        $(id("cat-35")).click(); //Категория Животные
        $(xpath("//*[@id=\"category-35\"]/div[2]/div[2]/div/ul/li[9]/a")).setSelected(true); // Подкатегория - Другие животные
        $(xpath("//*[@id=\"category-35\"]/div[2]/div[2]/div/ul/li[9]/a")).click(); // Подкатегория - Другие животные


        $(className("price ")).setValue("100");
        $(id("targetid_private_business")).click();
        $(linkText("Частное лицо")).click();
        $(id("add-description")).setValue(TEXT);

        $(id("mapAddress")).setValue("Иванковичи");


        $("#autosuggest-geo-ul li a").pressTab();

        $(id("add-phone")).setValue("+380955555555");

        $(id("save")).submit();


//        $(id("topLoginLink")).click();
//
//        boolean result = isDataExist();
//        if(result == false){
//
//            $(id("typewaiting")).click();
//            result = isDataExist();
//
//        }
//        Assert.assertTrue(result);
    }

    @AfterClass
    public void postCondition() {

        /** В связи с тем что время активации объявления может меняться (от нескольких секунд до минут)
         * данное условие периодически крешит тесты */
//        $(id("my-account-link")).click();
//        ElementsCollection resultActive = $$("#adsTable tr");
//        for (SelenideElement element : resultActive){
//            if(element.attr("data-title") != null && element.attr("data-title").contains(TITLE)) {
//                element.find(className("deactivateme")).click();
//                break;
//            }
//        }
    }

//    private boolean isDataExist(){
//        ElementsCollection resultActive = $$("#adsTable tr");
//        for (SelenideElement element : resultActive){
//            if(element.attr("data-title") != null && element.attr("data-title").contains(TITLE)) {
//                return  true;
//            }
//        }
//        return false;
//    }
}
